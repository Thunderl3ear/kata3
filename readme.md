# Kata 3 - A card deck shuffler
Small program that shuffles a deck of card and allows the user to draw cards (Y/n) input.

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Requirements

Requires `gcc`.

## Usage

Compile with g++ main.cpp -o build/shuffler
./build/shuffler

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## Contributing

## License

---
Copyright 2022, Thorbjørn Koch ([@Thunderl3ear](https://gitlab.com/Thunderl3ear))
