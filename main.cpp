// Author: Thorbjoern
// A card deck shuffle example program

#include<vector>
#include<iostream>
#include<algorithm>
#include<string>
#include<random>

struct a_card
{
    std::string value;
    std::string suit;
};



int main()
{
    std::vector<a_card> deck = {{"A","Clubs"},{"A","Spades"},{"A","Hearts"},{"A","Diamonds"},{"2","Clubs"},{"2","Spades"},{"2","Hearts"},{"2","Diamonds"},{"3","Clubs"},{"3","Spades"},{"3","Hearts"},{"3","Diamonds"},{"4","Clubs"},{"4","Spades"},{"4","Hearts"},{"4","Diamonds"},{"5","Clubs"},{"5","Spades"},{"5","Hearts"},{"5","Diamonds"},{"6","Clubs"},{"6","Spades"},{"6","Hearts"},{"6","Diamonds"},{"7","Clubs"},{"7","Spades"},{"7","Hearts"},{"7","Diamonds"},{"8","Clubs"},{"8","Spades"},{"8","Hearts"},{"8","Diamonds"},{"9","Clubs"},{"9","Spades"},{"9","Hearts"},{"9","Diamonds"},{"10","Clubs"},{"10","Spades"},{"10","Hearts"},{"10","Diamonds"},{"J","Clubs"},{"J","Spades"},{"J","Hearts"},{"J","Diamonds"},{"Q","Clubs"},{"Q","Spades"},{"Q","Hearts"},{"Q","Diamonds"},{"K","Clubs"},{"K","Spades"},{"K","Hearts"},{"K","Diamonds"}};
    // Add seed to get new random order each time.    
    std::random_device rd;
    std::mt19937 g(rd());
    
    std::shuffle(deck.begin(), deck.end(),g);
    int counter = 1; 
    for (a_card &card : deck)
    {
        std::cout<<"Card number "<<counter<<" is a "<<card.value<<" of "<<card.suit;
        counter++;
        std::cout<<". Draw another card (Y/n)";
        char input = std::cin.get();
        if(input=='n')
            break;
    }
    
    return 0;
}